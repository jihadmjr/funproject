# Funproject

Computing Finance Project using ReactJS and Haskell

Deployment is done manually using Docker as a Container, GCP, and Kubernetes.

URL FrontEnd : http://35.186.151.120/

URL BackEnd : http://35.186.151.120/

---

## Team Members
- Benny William Pardede - 1606917550
- Muhammad Imran Amrullah - 1606828942
- Muhammad Jihad Rinaldi - 1606883392
- Refo Ilmiya Akbar - 1606876033
- Tatag Aziz Prawiro - 1606875794

---

## Project Description
*Funproject* is a simple project to simulate installment regardin various consumer goods such as vehicles, real estates, etc. *Funproject* uses ReactJS as its front-end technology and Haskell as its back-end support.

---

## Dependencies
- node v12.13.0
- ghci v8.0.2
- cabal 1.24.0.2
- snap 1.0.0.0

---

## Installation Guide
This section would explain how to install and run the app for development
### Frontend
This section would explain how to install and run the frontend service of this project after you clone this repository.

1. `curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs`
2. `cd ./funproject`
3. `yarn`
4. `yarn start`

You can check the detailed `yarn` command explanation on the frontend README.MD

### Backend
This section would explain everything regarding the backend side of 
the project
#### Cabal and Happstack Installation
1. check for `cabal` existence by using `cabal --version` command on 
terminal.
2. If cabal is not installed, do the following `brew install cabal-install`
3. After cabal is installed to install HappStack, do the following 
command on your terminal:
    ``` 
    $ export PATH=~/.cabal/bin:$PATH
    $ cabal update
    $ cabal install happstack-server
    ```
4. If you have installed cabal and happstack, then you are ready to go!
5. To run the backend server, go to the backend folder from the root folder,
`cd backend` and then simply type `runhaskell main.hs`. 
6. If for some reason, there is an error about importing the calculate.hs 
file, just copy the whole code inside the calculate.hs into the main.hs file.

---

## API Documentation
This section would explain the 5 API routing for this project 
### Calculate Car Credit
An API to calculate the credit needed to be paid for a car.

Route:
```http
GET /api/calculate/car
```
Query Params:

| Key | Value | Description |
| :--- | :--- | :--- |
| `merk` | `string` | Your Car Vendor|
| `bunga` | `float` | Rate of Interest (yearly)|
| `harga` | `float` | Your Car Price|
| `durasi` | `float` | Your Credit Duration|    

Sample Request:

url: 
```http request
GET /api/calculate/car?tipe=Jazz 2002&bunga=0.06&harga=100000&durasi=6
``` 

Response [200] :
```http request
"16959.482,17298.672,17644.645,17997.537,18357.488,18724.639"
```

### Calculate Motorcycle Credit
An API to calculate the credit needed to be paid for a motorcycle.

Route:
```http
GET /api/calculate/motorcycle
```
Query Params:

| Key | Value | Description |
| :--- | :--- | :--- |
| `merk` | `string` | Your Motorcycle Vendor|
| `bunga` | `float` | Rate of Interest (yearly)|
| `harga` | `float` | Your Motorcycle Price|
| `durasi` | `float` | Your Credit Duration|    

Sample Request:

url: 
```http request
GET /api/calculate/motorcycle?tipe=Vario 2015&bunga=0.06&harga=100000&durasi=6
``` 

Response [200] :
```http request
"16959.482,17637.861,18343.375,19077.11,19840.193,20633.8"
```

### Calculate Phone Credit
An API to calculate the credit needed to be paid for a phone.

Route:
```http
GET /api/calculate/phone
```
Query Params:

| Key | Value | Description |
| :--- | :--- | :--- |
| `merk` | `string` | Your Phone Vendor|
| `bunga` | `float` | Rate of Interest (yearly)|
| `harga` | `float` | Your Phone Price|
| `durasi` | `float` | Your Credit Duration|    

Sample Request:

url: 
```http request
GET /api/calculate/phone?merk=Samsung&bunga=0.06&harga=100000&durasi=6
``` 

Response [200] :
```http request
"16959.482,18316.24,19781.54,21364.063,23073.188,24919.043"
```

### Calculate Laptop Credit
An API to calculate the credit needed to be paid for a laptop.

Route:
```http
GET /api/calculate/laptop
```
Query Params:

| Key | Value | Description |
| :--- | :--- | :--- |
| `merk` | `string` | Your Laptop Vendor|
| `bunga` | `float` | Rate of Interest (yearly)|
| `harga` | `float` | Your Laptop Price|
| `durasi` | `float` | Your Credit Duration|    

Sample Request:

url: 
```http request
GET /api/calculate/laptop?merk=Asus&bunga=0.06&harga=100000&durasi=6
``` 

Response [200] :
```http request
"16959.482,17977.05,19055.674,20199.014,21410.955,22695.613"
```

### Calculate Property Credit
An API to calculate the credit needed to be paid for a property.

Route:
```http
GET /api/calculate/property
```
Query Params:

| Key | Value | Description |
| :--- | :--- | :--- |
| `merk` | `string` | Your Property Type|
| `bunga` | `float` | Rate of Interest (yearly)|
| `harga` | `float` | Your Property Price|
| `durasi` | `float` | Your Credit Duration|    

Sample Request:

url: 
```http request
GET /api/calculate/property?merk=Apartemen&bunga=0.06&harga=100000&durasi=6
``` 

Response [200] :
```http request
"16959.482,17129.078,17300.37,17473.373,17648.107,17824.588"
```




