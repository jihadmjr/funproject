module Main where

import Calculate
import Control.Monad (msum)
import Happstack.Server
import System.Environment (getEnv)

calculateCarResponse =
     do merk <- look "merk"
        bunga <- look "bunga"
        harga <- look "harga"
        durasi <- look "durasi"
        ok $ toResponse (calculateCar merk bunga harga durasi)

calculateMotorcycleResponse =
     do merk <- look "merk"
        bunga <- look "bunga"
        harga <- look "harga"
        durasi <- look "durasi"
        ok $ toResponse (calculateMotorcycle merk bunga harga durasi)

calculatePhoneResponse =
     do merk <- look "merk"
        bunga <- look "bunga"
        harga <- look "harga"
        durasi <- look "durasi"
        ok $ toResponse (calculatePhone merk bunga harga durasi)

calculateLaptopResponse =
     do merk <- look "merk"
        bunga <- look "bunga"
        harga <- look "harga"
        durasi <- look "durasi"
        ok $ toResponse (calculateLaptop merk bunga harga durasi)

calculatePropertyResponse =
     do merk <- look "merk"
        bunga <- look "bunga"
        harga <- look "harga"
        durasi <- look "durasi"
        ok $ toResponse (calculateProperty merk bunga harga durasi)

main :: IO ()
main = simpleHTTP nullConf $ msum
        [ dir "api" $ dir "calculate" $ dir "car" $ calculateCarResponse
        , dir "api" $ dir "calculate" $ dir "motorcycle" $ calculateMotorcycleResponse
        , dir "api" $ dir "calculate" $ dir "phone" $ calculatePhoneResponse
        , dir "api" $ dir "calculate" $ dir "laptop" $ calculateLaptopResponse
        , dir "api" $ dir "calculate" $ dir "property" $ calculatePropertyResponse
        ]
