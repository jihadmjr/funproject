module Calculate where



calculateCar merk bunga harga durasi = show $ calculateCarCredit (calculateCredit (read(bunga) :: Float) (read(harga) :: Float) (read(durasi) :: Integer)) (read(durasi) :: Integer)
calculateMotorcycle merk bunga harga durasi = show $ calculateMotorcycleCredit (calculateCredit (read(bunga) :: Float) (read(harga) :: Float) (read(durasi) :: Integer)) (read(durasi) :: Integer)
calculatePhone merk bunga harga durasi = show $ calculatePhoneCredit (calculateCredit (read(bunga) :: Float) (read(harga) :: Float) (read(durasi) :: Integer)) (read(durasi) :: Integer)
calculateLaptop merk bunga harga durasi = show $ calculateLaptopCredit (calculateCredit (read(bunga) :: Float) (read(harga) :: Float) (read(durasi) :: Integer)) (read(durasi) :: Integer)
calculateProperty merk bunga harga durasi = show $ calculatePropertyCredit (calculateCredit (read(bunga) :: Float) (read(harga) :: Float) (read(durasi) :: Integer)) (read(durasi) :: Integer)

calculateCredit bunga harga durasi = harga / ((((1 + (bunga/12) ) ^ durasi) - 1)  / ((bunga/12) * (1 + (bunga/12) ) ^ durasi))

calculateCarCredit amount 1 = show(amount)
calculateCarCredit amount durasi = show(amount) ++ "," ++ (calculateCarCredit (coefCar amount) (durasi-1))
coefCar amount = amount + 0.02 * amount

calculateMotorcycleCredit amount 1 = show(amount)
calculateMotorcycleCredit amount durasi = show(amount) ++ "," ++ (calculateMotorcycleCredit (coefMotorcycle amount) (durasi-1))
coefMotorcycle amount = amount + 0.04 * amount

calculatePhoneCredit amount 1 = show(amount)
calculatePhoneCredit amount durasi = show(amount) ++ "," ++ (calculatePhoneCredit (coefPhone amount) (durasi-1))
coefPhone amount = amount + 0.08 * amount

calculateLaptopCredit amount 1 = show(amount)
calculateLaptopCredit amount durasi = show(amount) ++ "," ++ (calculateLaptopCredit (coefLaptop amount) (durasi-1))
coefLaptop amount = amount + 0.06 * amount

calculatePropertyCredit amount 1 = show(amount)
calculatePropertyCredit amount durasi = show(amount) ++ "," ++ (calculatePropertyCredit (coefProperty amount) (durasi-1))
coefProperty amount = amount + 0.01 * amount

