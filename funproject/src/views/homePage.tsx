import React, { useState } from 'react';
import Layout from "../components/Layout/layout"
import FormRow from "../components/FormRow/formrow"
import Label from '../components/Label/label'
import SelectForm from "../components/Select/selectForm"
import BungaSelect from "../components/Select/bungaSelect"
import List from "../components/List/list"
import dataJson from "../assets/data.json"
import * as api from "../helper/api"
import './homePage.css';


const HomePage: React.FC = () => {

    const [category, setCategory] = useState("0");
    const [result, setResult] = useState(['']);


    const changeCategory = (event:any) => {
        setCategory(event)
        localStorage.setItem("Category", event)
    };

    const getResult = () => {
        api.getCredit(dataJson.FormItem[parseInt(category)].toLowerCase(), localStorage.getItem("Merk"), 
        localStorage.getItem("bunga"), localStorage.getItem("Harga"), localStorage.getItem("Pinjaman"))
        let res = localStorage.getItem("result");
        if (res){
            setResult(res.split(","))
        }
        console.log(result)
    }

    
    return (
        <>
            <div>
                <Layout />
                <div className="homePage-Container">
                    <div className="homePage-Title">
                        <div className="home-title">
                            KREDIT.IN
                        </div>
                    </div>

                    <div className="Category-Dropdown">
                        <div className="Category-Label">
                            <Label text="Category"></Label>
                        </div>
                        <div>
                            <select className="Category-Select" onChange={e => changeCategory(e.target.value)}>
                                {dataJson.FormItem.map((data, idx) => {
                                    return (
                                        <option key={idx} value={idx}>{data}</option>
                                    )
                                })}
                            </select>
                        </div>
                    </div>
                    <div className="homePage-form">
                        <BungaSelect labelText={("BUNGA (%)")} bunga={dataJson.Bunga}></BungaSelect>

                        {(dataJson.Form[parseInt(category)]).map((data, idx) => {
                            if (data === "Tipe") {
                                return (
                                    <SelectForm key={idx} labelText={("Merk")} bunga={dataJson.Tipe}></SelectForm>
                                )

                            }
                            else if (data === "MerkMobil") {
                                return (
                                    <SelectForm key={idx} labelText={("Merk")} bunga={dataJson.MerkMobil}></SelectForm>
                                )
                            }
                            else if (data === "MerkMotor") {
                                return (
                                    <SelectForm key={idx} labelText={("Merk")} bunga={dataJson.MerkMotor}></SelectForm>
                                )
                            }
                            else if (data === "MerkLaptop") {
                                return (
                                    <SelectForm key={idx} labelText={("Merk")} bunga={dataJson.MerkLaptop}></SelectForm>
                                )
                            }
                            else if (data === "MerkHp") {
                                return (
                                    <SelectForm key={idx} labelText={("Merk")} bunga={dataJson.MerkHp}></SelectForm>
                                )
                            }
                            else {

                                return (
                                    <FormRow key={idx} formRowTitle={data} formRowType="number" required={true} />
                                )
                            }
                        })}
                    </div>
                    <div className="homePage-BtnCalc">
                        <button className="submit-button" onClick={e => getResult()}>
                            Calculate
                        </button>
                    </div>
                </div>
                <div className="homePage-Result">
                    <List content={result}></List>
                </div>
            </div>
        </>
    )
}

export default HomePage