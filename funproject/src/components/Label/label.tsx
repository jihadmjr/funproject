import React from 'react'
import './label.css'

type LabelProps = {
    text: string
}

const Label = (props: LabelProps) => {
    return (
        <label className="label-text">
            {props.text || 'Label'}
        </label>
    )
}

export default Label;