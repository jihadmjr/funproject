import React from './node_modules/react';
import { linkTo } from './node_modules/@storybook/addon-links';
import { Welcome } from './node_modules/@storybook/react/demo';

export default {
  title: 'Welcome',
};

export const toStorybook = () => <Welcome showApp={linkTo('Button')} />;

toStorybook.story = {
  name: 'to Storybook',
};
