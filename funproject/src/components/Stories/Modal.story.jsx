import React from "./node_modules/react";
import { storiesOf } from "./node_modules/@storybook/react";

import Modal from "../Modal/Modal";

storiesOf("Modal").add("Default Modal", () => (
  <Modal
    visibility={true}
    onClose={() => null}
    onPrev={() => null}
    onNext={() => null}
    hasNext={false}
    hasPrev={false}
  >
    {" "}
    Modal{" "}
  </Modal>
));
