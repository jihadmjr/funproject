import React from 'react'
import "./button.css"

type AppProps = {
    content : String,

}
type AppState = {

}

const Button = (props: AppProps) => {
    return(
        <div>
            <button className="submit-button">
                {props.content}
            </button>
        </div>
    )
}

export default Button