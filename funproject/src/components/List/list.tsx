import React from 'react'
import "./list.css"

type AppProps = {
    content: string[]
}

const List = ({ content }: AppProps) => {
    const body = content.map(renderCell)
    console.log(content)
    if(content[0] !== ''){
        return (
            <div className={"list-class"}>
                <h4 className={"title"}>Angsuran {localStorage.getItem("Merk")} Selama {localStorage.getItem("Pinjaman")} Bulan</h4>
            <table>
                {body}
            </table>
        </div>
        )
    } else {
        return(
            <div></div>
        )
    }
}

const renderCell = (cellContent: string, idx: number) => {
    const title = "Bulan ke " + (idx + 1);
    var rupiah = formatRupiah(Math.floor(parseInt(cellContent)).toString(), 'Rp. ');
    return (
        <tbody key={idx}>
        <tr>
            <td>{title}</td>
            <td>{rupiah}</td>
        </tr>
        </tbody>
        
    )
}

function formatRupiah(angka: string, prefix: string) {
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
    split   		= number_string.split(','),
    sisa     		= split[0].length % 3,
    rupiah     		= split[0].substr(0, sisa),
    ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    if(ribuan){
        var separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

export default List