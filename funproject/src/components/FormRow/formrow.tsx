import React from 'react'
import Label from '../Label/label'
import './formRow.css' 

type AppProps = {
    formRowTitle : string,
    formRowType : string,
    required : boolean
}
const FormRow = ({ formRowTitle, formRowType, required } : AppProps) => {
    const updateInputValue = (event:any) => {
        if(formRowTitle.split(" ").length > 1){
            localStorage.setItem(formRowTitle.split(" ")[1], event.target.value)
        } else{
            localStorage.setItem(formRowTitle, event.target.value)
        }
    }
    return(
        <div className="formRow-container">
            <div>
                <Label text={formRowTitle}></Label>
            </div>
            <div className="formRow-inputContainer">
                <input id="formInput" onChange={e => updateInputValue(e)} className="formRow-input" type={formRowType}  required={required} min={0}/>
            </div>
        </div>
    )
}

export default FormRow;