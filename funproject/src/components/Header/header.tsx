import React, {Component} from 'react'
import './header.css';

type AppProps = {

}
type AppState = {

}

export default function Header(){
    return(
        <div>
            <div className="header-container">
                <div className="header-user-section-name">
                        <button className="login-button">
                            FunProject
                        </button>
                </div>
            </div>
        </div>
    )
}
