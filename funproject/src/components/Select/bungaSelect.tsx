import React from 'react'
import Label from '../Label/label'
import './selectForm.css'

type SelectProps = {
    bunga: number[]
    labelText: string
}

const BungaSelect = (props: SelectProps) => {
    
    const selectOnchange = (event:any) => {
        localStorage.setItem("bunga", event)
    }
    
    return (
        <div className ="selectForm-container">
            <div>
                <Label text={props.labelText} ></Label>
            </div>
            <div>
                <select className="selectForm-item" onChange={e => selectOnchange(e.target.value)}>
                    {props.bunga.map((data, idx) => {
                        return(
                            <option key={idx} value={data/100}>{data}</option>
                        )
                    })}
                </select>
            </div>

        </div>

    )
}

export default BungaSelect;