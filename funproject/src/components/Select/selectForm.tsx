import React from 'react'
import Label from '../Label/label'
import './selectForm.css'

type SelectProps = {
    bunga: string[]
    labelText: string
}

const SelectForm = (props: SelectProps) => {

    const selectOnchange = (event:any) => {
        localStorage.setItem(props.labelText, event)
    }


    return (
        <div className ="selectForm-container">
            <div>
                <Label text={props.labelText} ></Label>
            </div>
            <div>
                <select className="selectForm-item" onChange={e => selectOnchange(e.target.value)}>
                    {props.bunga.map((data, idx) => {
                        return(
                            <option key={idx} value={data}>{data}</option>
                        )
                    })}
                </select>
            </div>

        </div>

    )
}

export default SelectForm;