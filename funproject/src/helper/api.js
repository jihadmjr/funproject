
const url = "https://cors-anywhere.herokuapp.com/35.247.158.105/api/calculate/";
// url + category + "?merk= " + merk + "&bunga=" + bunga + "&harga=" + harga + "&durasi=" + durasi
export const getCredit = (category, merk, bunga, harga, durasi) => {
    let source = url + category + "?merk=" + merk + "&bunga=" + bunga + "&harga=" + harga + "&durasi=" + durasi
    console.log(source)
    let header = new Headers(
        {
            'Content-Type':'application/json',
            'Access-Control-Allow-Origin':'*',
            'Access-Control-Request-Headers':'x-requested-with',
        }
    )
    fetch(source,
    {
        crossDomain: true,
        method: 'GET',
        cache: 'no-cache',
        headers: header,
    })
    .then(
        function(response) {
            if(response.status !== 200){
                console.log("Error");
                return
            }
            response.json().then(function(data){
                localStorage.setItem("result", data)
                console.log(data)
                return(data)
            });
        }
    )
    .catch(function(err){
        console.log('Fetch Error :-S', err)
    })
}