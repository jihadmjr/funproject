import React from 'react';
import './App.css';
import HomePage from "./views/homePage";
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';

const App: React.FC = () => {
  return (
    <div className="App">
      <Router>
        <div>
          <Route path="/" component={HomePage}/>
        </div>
      </Router>
    </div>
  );
}

export default App;
