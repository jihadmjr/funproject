import { configure } from '@storybook/react';

// automatically import all files ending in *.stories.js *.stories.
// configure(require.context('../stories', true, /\.stories\.js$/), module);

// import all stories files to storybook
configure(require.context('../src/components/stories', true, /\.stories\.js$/), module);
configure(require.context('../src/components/stories', true, /\.story\.jsx$/), module);
configure(require.context('../src/components/stories', true, /\.stories\.ts$/), module);
configure(require.context('../src/components/stories', true, /\.story\.tsx$/), module);
